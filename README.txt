A Pen created at CodePen.io. You can find this one at http://codepen.io/mcnemesis/pen/EgWEPg.

 Adapted this as the basis for the auto-analytics component of the futuristic data platform called Project Okot (https://nuchwezi.com)

---------------
By [CSSFlow](http://www.cssflow.com): free UI elements and widgets coded with HTML5, CSS3, and Sass.  

View the original article and download the Sass source at:  
[cssflow.com/snippets/analytics-widget](http://www.cssflow.com/snippets/analytics-widget)

Original PSD by [Farzad Ban](http://www.premiumpixels.com/freebies/analytics-widget-psd/).

Tested in Firefox 4, Safari 4, Chrome 14, Opera 10, IE 8 (and newer).